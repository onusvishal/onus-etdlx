/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, AsyncStorage, StatusBar, AppState } from 'react-native';
import { Root } from 'native-base';
import { createStackNavigator } from 'react-navigation';
import { Actions } from 'react-native-router-flux';

import { Spinner } from './src/common';
import Navigator from './src/action/Navigator';
import NoInternet from './src/helper/NoInternet';
import { IS_LOGIN } from './src/helper/Constant';

type Props = {};
export default class App extends Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLogin: false,
      isConnected: true,
      fromLink: false,
      appState: AppState.currentState
    };
 }

  componentDidMount() {
    AsyncStorage.getItem(IS_LOGIN).then((value) => {
      if (value == 'true') {
        this.setState({ loading: false, isLogin: true });
      } else {
        this.setState({ loading: false, isLogin: false });
      }
    });
  }

  spinerRender() {
    if (this.state.isConnected) {
        if (this.state.loading) {
          return (
            <View style={styles.container}><Spinner size="large" /></View>
          );
        }
          return (
          <View style={{ flex: 1 }}>
            <Navigator isLogin={this.state.isLogin} />
          </View>
          );
    }
    return (
        <NoInternet checkConnection={this.checkConnection} />
      );
  }

  render() {
    return (
      <Root style={styles.container}>
          <StatusBar
            backgroundColor="#FFFFFF"
            barStyle="dark-content"
          />
          {this.spinerRender()}
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

