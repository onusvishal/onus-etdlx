import React from 'react';
import { View, Text, Image } from 'react-native';
import { imageNOInternet } from './Constant';
import { Button } from '../common';

const NoInternet = (props) => {
	const { checkConnection } = props;
	return (
		<View style={styles.container}>
			<Image source={imageNOInternet} resizeMode={Image.resizeMode.center} />
			<Text style={styles.textStyle}> { "Pas de connexion internet" } </Text>
			<Text style={styles.textStyle}> { "Merci de vérifier le réseau et d'essayer à nouveau." } </Text>
			<Button text="Retry" onPress={checkConnection} />
		</View>
	);
};

const styles = {
  container: {
    flex: 1,
    padding: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
  },
  textStyle: {
    fontSize: 16,

   }
};
export default NoInternet;
