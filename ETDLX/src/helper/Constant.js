export const ALL_FIELDS_REQURED = 'champ est vide';


// Error Message Log
export const ENTER_VALID_EMAIL = 'Le mail semble invalide';

export const ENTER_VALID_PASSWORD = 'Le mot de passe semble erroné';
export const ENTER_OLD_PASSWORD = 'Entrez le précédent mot de passe';

export const IS_LOGIN = 'ETDLX_isLogin';

//arrays
export const menuArray = ['ACCUEIL', 'HISTORIQUE', 'MES DEMANDES',
 'MES RÉGULATEURS', 'ACHETER DES CREDITS', 'MON COMPTE', 'DESTRESS AMBULANCES',
 'CONDITIONS GENERALES DUTILISATION', 'AIDE', 'CONTACT/SUPPORT', 'DECONNECTION'
];

// export const countryCode = [54,1,598,81,56,54,7,56,1,56,672,55,
//     72,672,81,33,54,34,49,56,886,44,48,82,92,34,54,82,49,852,40,7,51,672,1,54,39,672,1,420,7,81,382,49,7,54,1,
//     32,56,7,44,7,54,27,44,359,64,81,46,47,47,46,7,380,86];

export const countryCode = [+54,+1,+598,+81,+56,+7,+672,+55,+39,+33,+34];

// COLOR CODE
export const BACKGROUND_COLOR = '#FFFFFF';
export const TEXT_COLOR = '#212121';
export const BORDER_COLOR = '#212121';
export const ERROR_COLOR = '#ff0000';

export const BLUE_COLOR = '#2b3990';
export const RED_COLOR = '#ff0000';
export const GREEN_COLOR = '#69b505';
export const WHITE_COLOR = '#FFFFFF';
export const TRANSPARENT_COLOR = 'transparent';
export const NORMAL_FONT = 'FuturaLT';
export const BOLD_FONT = 'FuturaLT-Bold';

// BASE URL
export const BASE_URL = 'https://lab.destress-ambulances.fr';

// USER
export const LOGIN_URL = BASE_URL + '/api/user/login';
export const FIRSTTIME_LOGIN_URL = BASE_URL + '/api/check_user';
export const FORGOT_PASSWORD_URL = BASE_URL + '/api/user/forgot-password';
export const REGULATEUR_REGISTRATION_URL = BASE_URL + '/api/user/register';
export const EDIT_USERPROFILE  = BASE_URL + '/api/user/edit-profile';
export const EDIT_PROFILEIMAGE_URL = BASE_URL + '/api/user/profile-update';

// AMBULANCE
export const AMBULANCE_REGISTRATION_URL = BASE_URL + '/api/add-ambulance';
export const GETZONE_URL = BASE_URL + '/api/get-zones';
export const GETZIPCODE_URL = BASE_URL + '/api/get-zipcode';
export const GET_AMBULANCE_N_USER = BASE_URL + '/api/get-ambulance-and-user';
export const EDIT_AMBULANCE = BASE_URL + '/api/edit-ambulance';
export const CHANGE_USER_STATUS = BASE_URL + '/api/change-user-status';
export const GETNOTIFICATION_PERMISSION = BASE_URL + '/api/get-notification';

// DEMANDE
export const ADD_DEMAND_URL = BASE_URL + '/api/add-demande';
export const GET_CRITERIA = BASE_URL + '/api/get-criteria';
export const DEMAND_LIST_URL = BASE_URL + '/api/demande-list';
export const GET_DEMAND_HISTORY = BASE_URL + '/api/demande-history';
export const VIEW_DEMAND_URL = BASE_URL + '/api/demande-view';

export const APPLY_COMPLETE_DEMAND = BASE_URL + '/api/complete-demand';
export const GET_MY_DEMAND_LIST = BASE_URL + '/api/mes-demande';
export const ADD_DEMAND_WINNER = BASE_URL + '/api/select-winner';
export const CANCEL_MY_DEMAND = BASE_URL + '/api/cancel-demand';
export const GET_DEMAND_REGULATORLIST = BASE_URL + '/api/get-regulator';
export const DEMAND_APPLY_URL = BASE_URL + '/api/demand-apply';
export const REJECT_DEMAND_URL = BASE_URL + '/api/user-reject-demand';

// PAYMENT
export const POST_SENDIRECT_URL = BASE_URL + '/api/send-redirect-flow-url';
export const GET_CREDIT_LIST = BASE_URL + '/api/get-tarif';
export const PAYMENT_URL = BASE_URL + '/api/payment';

export const LOGOUT_URL = BASE_URL + '/api/user/logout';


//Images

export const imageNOInternet = require('../../assets/image/no_internet.png');
export const CameraImage = require('../../assets/image/camera.png');
export const RecordVideoImage = require('../../assets/image/rec.png');
export const FileImage = require('../../assets/image/add-file.png');