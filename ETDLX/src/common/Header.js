import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
	return (
		<View style={styles.container}>
			<Text style={styles.textStyle}> {props.title} </Text>
		</View>
	);
};

const styles = {
	container: {
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#3498db',
		height: 60,
		paddingTop: 10,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.5,
		elevation: 2,
		position: 'relative'
	},

	textStyle: {
		fontSize: 20,
		fontFamily: 'Asap-Regular'
	}
};
export { Header };
