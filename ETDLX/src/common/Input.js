import React, { Component } from 'react';
import { TextInput, Text, View } from 'react-native';

class Input extends Component {

	constructor(props) {
		super(props);
		this.focusNextField = this.focusNextField.bind(this);
		this.inputs = {};
	}

	focusNextField(id) {
    this.inputs[id].focus();
  }

	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.lableStyle}>{this.props.label}</Text>
				<TextInput
					keyboardType={this.props.keyboardType}
					secureTextEntry={this.props.secureTextEntry}
					placeholder={this.props.placeHolder}
					autoCorrect={false}
					value={this.props.value}
					onChangeText={this.props.onChangeText}
					underlineColorAndroid={this.props.borderColor}
					style={styles.textInputStyle}
				/>
			</View>
		);
	}
}

const styles = {
	container: {
		flexDirection: 'row',
		flex: 1,
		height: 40,
		alignItems: 'center'
	},

	lableStyle: {
		flex: 1,
		fontSize: 18,
		paddingLeft: 20,
	},

	textInputStyle: {
		flex: 2,
		fontSize: 18,
		paddingLeft: 5,
		paddingRight: 5,
	}
};

export { Input };
