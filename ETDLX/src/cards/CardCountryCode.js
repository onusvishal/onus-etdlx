import React from 'react';
import { View, Text } from 'react-native';

const CardCountryCode = (props) => {
    return (
        <View>
            {
                Platform.OS == 'ios' ?
                    <View style={{ width: 60, borderBottomWidth: 1, borderColor: 'lightgrey' }}>
                        <Picker
                            note
                            mode="dropdown"
                            style={{ width: 60 }}
                            selectedValue={this.state.selectCode}
                            onValueChange={(value) => this.setState({ selectCode: value })} >
                            <Picker.Item label="+911" value="+911" />
                            <Picker.Item label="+92" value="+92" />
                            <Picker.Item label="+93" value="+93" />
                            <Picker.Item label="+94" value="+94" />
                            <Picker.Item label="+95" value="+95" />
                        </Picker>
                    </View>
                    :
                    <View style={{ width: 100, borderBottomWidth: 1, borderColor: 'lightgrey' }}>
                        <Picker
                            style={{ width: 100 }}
                            selectedValue={this.state.selectCode}
                            onValueChange={(value) => this.setState({ selectCode: value })} >
                            <Picker.Item label="+91" value="+911" />
                            <Picker.Item label="+92" value="+92" />
                            <Picker.Item label="+93" value="+93" />
                        </Picker>
                    </View>
            }
        </View>
    );
};

export default CardCountryCode;
