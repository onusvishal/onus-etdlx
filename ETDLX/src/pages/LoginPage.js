import React, { Component } from 'react';
import {
    StyleSheet, View, TouchableOpacity,Platform,
    ScrollView, SafeAreaView, Keyboard, KeyboardAvoidingView
} from 'react-native';
import { Form, Input, Item, Text } from 'native-base';
import axios from 'axios';

import { ALL_FIELDS_REQURED, BACKGROUND_COLOR, TEXT_COLOR, BORDER_COLOR, ERROR_COLOR } from '../helper/Constant';
import { Actions } from 'react-native-router-flux';
class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            message: '',
            loading: false,
            inputEmailError: false,
            inputPasswordError: false,
        };
        this.checkValidation = this.checkValidation.bind(this);
        this.LogInAPICall = this.LogInAPICall.bind(this);
    }

    onLoginButtonPressed() {
        this.checkValidation();
    }

    onSignupButtonPressed() {
        // Actions.Dashbord();
        Actions.SignupPage();
    }

    checkValidation() {
        if (this.state.email !== '' && this.state.password !== '') {
            this.LogInAPICall();
            this.setState({
                inputEmailError: false,
                inputPasswordError: false,
                message: ''
            });
        } else {
            if (this.state.email === '') {
                this.setState({
                    inputEmailError: true,
                });
            } else {
                this.setState({
                    inputEmailError: false,
                });
            }
            if (this.state.password === '') {
                this.setState({
                    inputPasswordError: true
                });
            } else {
                this.setState({
                    inputPasswordError: false,
                });
            }
            this.setState({
                message: ALL_FIELDS_REQURED
            });
        }
    }

    LogInAPICall() {
        Keyboard.dismiss();
        alert('Success');
    }

    spinerRender() {
        if (this.state.loading) {
            return <Spinner size="large" />;
        }
        return (
            <TouchableOpacity onPress={this.onLoginButtonPressed.bind(this)}>
                <View style={styles.VIEW_Button}>
                    <Text style={styles.TXT_Button} > CONNEXION </Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        const { VIEW_Container, VIEW_Flex, TXT_Header, VIEW_Center,
            VIEW_Bottom, TXT_Button, TXT_Message, VIEW_Button } = styles;
        return (
            <SafeAreaView style={VIEW_Flex}>
                <KeyboardAvoidingView 
                style={VIEW_Flex} 
                enabled
                behavior= {(Platform.OS === 'ios')? "padding" : null} 
                keyboardVerticalOffset={Platform.select({ios: 0, android: 500})}
                >
                    <View style={VIEW_Flex}>
                        <ScrollView>
                            <View style={VIEW_Container}>
                                <View style={VIEW_Center}>
                                    <Text style={[TXT_Header, { marginTop: 100 }]}>ETDLX</Text>
                                </View>

                                <View style={{ height: 30 }} />
                                <Form style={styles.INPUT_Container}>
                                    <Item
                                        floatingLabel
                                        error={this.state.inputEmailError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='EMAIL'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            keyboardType={'email-address'}
                                            onSubmitEditing={() => { this.password._root.focus(); }}
                                            onChangeText={(value) => this.setState({ email: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputPasswordError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='MOT DE PASSE'
                                            getRef={(input) => { this.password = input; }}
                                            onChangeText={(value) => this.setState({ password: value })}
                                            doneButtonTitle={'OK'}
                                            autoCapitalize='none'
                                            secureTextEntry />
                                    </Item>
                                </Form>

                                <Text style={TXT_Message}>
                                    {this.state.message}
                                </Text>
                                <View style={{ height: 20 }} />
                                {this.spinerRender()}
                            </View>
                            <View>
                                <View style={VIEW_Bottom}>
                                    <TouchableOpacity
                                        onPress={this.onSignupButtonPressed.bind(this)} >
                                        <View style={VIEW_Button}>
                                            <Text style={TXT_Button} > CRÉER UN COMPTE </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    VIEW_Flex: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR
    },
    VIEW_Center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    VIEW_Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 40,
        paddingRight: 30,
        paddingBottom: 20,
        paddingTop: 20,
    },
    TXT_Header: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
    },
    TXT_Button: {
        alignSelf: 'center',
        color: TEXT_COLOR,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 20,
    },
    TXT_Message: {
        color: ERROR_COLOR,
        fontSize: 16,
        marginTop: 10,
    },
    INPUT_Container: {
        padding: 0,
        alignSelf: 'stretch',
    },
    VIEW_Bottom: {
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 0,
        height: 100,
    },
    VIEW_Button: {
        borderRadius: 8,
        height: 40,
        justifyContent: 'center',
        borderColor: BORDER_COLOR,
        borderWidth: 0.5,
    },

});

export default LoginPage;
