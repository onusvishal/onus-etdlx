import React, { Component } from 'react';
import {
    StyleSheet, View, TouchableOpacity, Platform,
    ScrollView, SafeAreaView, Keyboard, KeyboardAvoidingView
} from 'react-native';
import { Form, Input, Item, Text } from 'native-base';
import axios from 'axios';

import { ALL_FIELDS_REQURED, BACKGROUND_COLOR, TEXT_COLOR, BORDER_COLOR, ERROR_COLOR } from '../helper/Constant';
import { Actions } from 'react-native-router-flux';
class Dashbord extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pays: '',
            number: '',
            email: '',
            message: '',
            loading: false,
            inputPayError: false,
            inputNumberError: false,
            inputEmailError: false,
        };
        this.checkValidation = this.checkValidation.bind(this);
        this.SearchAPICall = this.SearchAPICall.bind(this);
    }

    onLoginButtonPressed() {
        this.checkValidation();
    }

    onSignupButtonPressed() {
        // Actions.Dashbord();
        Actions.SignupPage();
    }

    checkValidation() {
        if (this.state.pays !== '' && this.state.email !== '' || this.state.number !== '' ) {
            this.SearchAPICall();
            this.setState({
                inputEmailError: false,
                inputNumberError: false,
                inputPayError: false,
                message: ''
            });
        } else {
            if (this.state.pays === '') {
                this.setState({
                    inputPayError: true
                });
            } else {
                this.setState({
                    inputPayError: false,
                });
            }
            if (this.state.number === '' && this.state.email === '') {
                this.setState({
                    inputNumberError: true,
                    inputEmailError: true,
                });
            } else if (this.state.number === '' && this.state.email !== '' || this.state.number !== '' && this.state.email === '') {
                this.setState({
                    inputNumberError: false,
                    inputEmailError: false,
                });
            }
            this.setState({
                message: ALL_FIELDS_REQURED
            });
        }
    }

    SearchAPICall() {
        Keyboard.dismiss();
        alert('Success');
    }

    spinerRender() {
        if (this.state.loading) {
            return <Spinner size="large" />;
        }
        return (
            <TouchableOpacity onPress={this.onLoginButtonPressed.bind(this)}>
                <View style={styles.VIEW_Button}>
                    <Text style={styles.TXT_Button} > Chercher </Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        const { VIEW_Container, VIEW_Flex, TXT_Header, VIEW_Center,
            VIEW_Bottom, TXT_Button, TXT_Message, VIEW_Button } = styles;
        return (
            <SafeAreaView style={VIEW_Flex}>
                <KeyboardAvoidingView
                    style={VIEW_Flex}
                    enabled
                    behavior={(Platform.OS === 'ios') ? "padding" : null}
                    keyboardVerticalOffset={Platform.select({ ios: 0, android: 500 })}
                >
                    <View style={VIEW_Flex}>
                        <ScrollView>
                            <View style={VIEW_Container}>
                                <View style={{ height: 30 }} />
                                <Form style={styles.INPUT_Container}>
                                    <Item
                                        floatingLabel
                                        error={this.state.inputPayError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Pays'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.number._root.focus(); }}
                                            onChangeText={(value) => this.setState({ pays: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputNumberError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Number De téléphone'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            keyboardType={'phone-pad'}
                                            getRef={(input) => { this.number = input; }}
                                            onChangeText={(value) => this.setState({ number: value })}
                                             />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <View style={VIEW_Center}>
                                        <Text style={[TXT_Header]}>or</Text>
                                    </View>
                                    <Item
                                        floatingLabel
                                        error={this.state.inputEmailError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='E-mail'
                                            autoCapitalize='none'
                                            doneButtonTitle={'OK'}
                                            keyboardType={'email-address'}
                                            onChangeText={(value) => this.setState({ email: value })}
                                             />
                                    </Item>
                                </Form>

                                <Text style={TXT_Message}>
                                    {this.state.message}
                                </Text>
                                <View style={{ height: 20 }} />
                                {this.spinerRender()}
                            </View>
                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    VIEW_Flex: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR
    },
    VIEW_Center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    VIEW_Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 40,
        paddingRight: 30,
        paddingBottom: 20,
        paddingTop: 20,
    },
    TXT_Header: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
    },
    TXT_Button: {
        alignSelf: 'center',
        color: TEXT_COLOR,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 20,
    },
    TXT_Message: {
        color: ERROR_COLOR,
        fontSize: 16,
        marginTop: 10,
    },
    INPUT_Container: {
        padding: 0,
        alignSelf: 'stretch',
    },
    VIEW_Bottom: {
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 0,
        height: 100,
    },
    VIEW_Button: {
        borderRadius: 8,
        height: 40,
        justifyContent: 'center',
        borderColor: BORDER_COLOR,
        borderWidth: 0.5,
    },

});

export default Dashbord;
