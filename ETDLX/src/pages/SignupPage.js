import React, { Component } from 'react';
import {
    StyleSheet, View, TouchableOpacity,
    ScrollView, SafeAreaView, Keyboard, KeyboardAvoidingView, Platform, TextInput
} from 'react-native';
import { Form, Input, Item, Text, Icon, Picker } from 'native-base';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import CardCountryCode from '../cards/CardCountryCode';

import { ALL_FIELDS_REQURED, BACKGROUND_COLOR, TEXT_COLOR, BORDER_COLOR, ERROR_COLOR } from '../helper/Constant';
class SignupPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            firstname: '',
            mobile: '',
            email: '',
            password: '',
            pays: '',
            type: '',
            number: '',
            street: '',
            street1: '',
            street2: '',
            selectCode: '+911',
            message: '',
            loading: false,
            inputNameError: false,
            inputFirstNameError: false,
            inputMobileError: false,
            inputEmailError: false,
            inputPasswordError: false,
            inputPaysError: false,
            inputTypeError: false,
            inputNumberError: false,
            inputStreetError: false,
            inputStreet1Error: false,
            inputStreet2Error: false,
        };
        this.checkValidation = this.checkValidation.bind(this);
        this.SignupAPICall = this.SignupAPICall.bind(this);
    }

    onLoginButtonPressed() {
        this.checkValidation();
    }

    onSignupButtonPressed() {

    }

    renderCountryCode() {
        // if (this.state.data && this.state.data.length > 0) {
        //   return this.state.data.map((item, index) =>
        <CardCountryCode
            // key={`key-${index}`}
        />
        // );
        // }
    }

    checkValidation() {
        if (this.state.email !== '' && this.state.name && this.state.firstname &&
            this.state.password !== '' && this.state.pays !== '' && this.state.type !== '' &&
            this.state.number !== '' && this.state.street !== '' && this.state.street1 !== '' &&
            this.state.street2 !== '') {
            this.SignupAPICall();
            this.setState({
                inputEmailError: false,
                inputPasswordError: false,
                inputFirstNameError: false,
                inputNameError: false,
                inputMobileError: false,
                inputPaysError: false,
                inputTypeError: false,
                inputNumberError: false,
                inputStreetError: false,
                inputStreet1Error: false,
                inputStreet2Error: false,
                message: ''
            });
        } else {
            if (this.state.name === '') {
                this.setState({
                    inputNameError: true,
                });
            } else {
                this.setState({
                    inputNameError: false,
                });
            }
            if (this.state.firstname === '') {
                this.setState({
                    inputFirstNameError: true,
                });
            } else {
                this.setState({
                    inputFirstNameError: false,
                });
            }
            if (this.state.mobile === '') {
                this.setState({
                    inputMobileError: true,
                });
            } else {
                this.setState({
                    inputMobileError: false,
                });
            }
            if (this.state.email === '') {
                this.setState({
                    inputEmailError: true,
                });
            } else {
                this.setState({
                    inputEmailError: false,
                });
            }
            if (this.state.password === '') {
                this.setState({
                    inputPasswordError: true
                });
            } else {
                this.setState({
                    inputPasswordError: false,
                });
            }

            if (this.state.pays === '') {
                this.setState({
                    inputPaysError: true
                });
            } else {
                this.setState({
                    inputPaysError: false,
                });
            }
            if (this.state.type === '') {
                this.setState({
                    inputTypeError: true
                });
            } else {
                this.setState({
                    inputTypeError: false,
                });
            }
            if (this.state.number === '') {
                this.setState({
                    inputNumberError: true
                });
            } else {
                this.setState({
                    inputNumberError: false,
                });
            }
            if (this.state.street === '') {
                this.setState({
                    inputStreetError: true
                });
            } else {
                this.setState({
                    inputStreetError: false,
                });
            }
            if (this.state.street1 === '') {
                this.setState({
                    inputStreet1Error: true
                });
            } else {
                this.setState({
                    inputStreet1Error: false,
                });
            }
            if (this.state.street2 === '') {
                this.setState({
                    inputStreet2Error: true
                });
            } else {
                this.setState({
                    inputStreet2Error: false,
                });
            }

            this.setState({
                message: ALL_FIELDS_REQURED
            });
        }
    }

    SignupAPICall() {
        Keyboard.dismiss();
        alert('Success');
    }

    spinerRender() {
        if (this.state.loading) {
            return <Spinner size="large" />;
        }
        return (
            <TouchableOpacity onPress={this.onLoginButtonPressed.bind(this)}>
                <View style={styles.VIEW_Button}>
                    <Text style={styles.TXT_Button} > ENREGISTRER </Text>
                </View>
            </TouchableOpacity>
        );
    }

    onChange(value) {
        this.setState({
            selected: value
        });
    }

    render() {
        const { VIEW_Container, VIEW_Flex, TXT_Header, VIEW_Center, VIEW_Left,
            TXT_Message, VIEW_ROW, VIEW_Header, BTN_BACK } = styles;
        return (
            <SafeAreaView style={VIEW_Flex}>
                <KeyboardAvoidingView
                    style={VIEW_Flex}
                    enabled
                    behavior={(Platform.OS === 'ios') ? "padding" : null}
                    keyboardVerticalOffset={Platform.select({ ios: 0, android: 500 })}
                >
                    <View style={VIEW_Flex}>
                        <View style={[VIEW_Header, { justifyContent: 'center', alignItems: 'center' }]}>
                            <Icon
                                type='Entypo'
                                name='chevron-left'
                                style={BTN_BACK}
                                onPress={() => Actions.pop()} />
                            <View style={VIEW_Flex}>
                                <Text style={[TXT_Header]}>ETDLX</Text>
                            </View>
                            <View style={{ margin: 16, height: 25, width: 25 }} />
                        </View>

                        <ScrollView>
                            <View style={VIEW_Container}>

                                <Form style={styles.INPUT_Container}>
                                    <Item
                                        floatingLabel
                                        error={this.state.inputNameError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Nom'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.firstname._root.focus(); }}
                                            onChangeText={(value) => this.setState({ name: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputFirstNameError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Prénom'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.email._root.focus(); }}
                                            getRef={(input) => { this.firstname = input; }}
                                            onChangeText={(value) => this.setState({ firstname: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                </Form>

                                <View style={{ height: 50 }} />
                
                                <View style={[VIEW_ROW, { marginLeft: 5, marginRight: 10 }]}>

                                    {
                                        Platform.OS == 'ios' ?
                                            <View style={{ width: 60, borderBottomWidth: 1, borderColor: 'lightgrey' }}>
                                                <Picker
                                                    note
                                                    mode="dropdown"
                                                    style={{ width: 60 }}
                                                    selectedValue={this.state.selectCode}
                                                    onValueChange={(value) => this.setState({ selectCode: value })} >
                                                    <Picker.Item label="+911" value="+911" />
                                                    <Picker.Item label="+92" value="+92" />
                                                    <Picker.Item label="+93" value="+93" />
                                                    <Picker.Item label="+94" value="+94" />
                                                    <Picker.Item label="+95" value="+95" />
                                                </Picker>
                                            </View>
                                            :
                                            <View style={{ width: 100, borderBottomWidth: 1, borderColor: 'lightgrey' }}>
                                                <Picker
                                                    style={{ width: 100 }}
                                                    selectedValue={this.state.selectCode}
                                                    onValueChange={(value) => this.setState({ selectCode: value })} >
                                                    <Picker.Item label="+91" value="+911" />
                                                    <Picker.Item label="+92" value="+92" />
                                                    <Picker.Item label="+93" value="+93" />
                                                </Picker>
                                            </View>
                                    }
                                    <View style={{ width: 5 }} />
                                    <View style={{ flex: 1 }}>
                                        <TextInput
                                            placeholder='Number De téléphone'
                                            placeholderTextColor='#636363'
                                            keyboardType={'phone-pad'}
                                            maxLength={10}
                                            style={{ height: 50, borderColor: 'lightgrey', borderBottomWidth: 1.0, fontSize: 16 }}
                                            onChangeText={(value) => this.setState({ mobile: value })}
                                            value={this.state.mobile}
                                        />
                                    </View>
                                </View>

                                <View style={{ height: 20 }} />
                                <Form style={styles.INPUT_Container}>
                                    <Item
                                        floatingLabel
                                        error={this.state.inputEmailError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='EMAIL'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            keyboardType={'email-address'}
                                            onSubmitEditing={() => { this.password._root.focus(); }}
                                            getRef={(input) => { this.email = input; }}
                                            onChangeText={(value) => this.setState({ email: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputPasswordError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='MOT DE PASSE'
                                            onSubmitEditing={() => { this.pays._root.focus(); }}
                                            getRef={(input) => { this.password = input; }}
                                            onChangeText={(value) => this.setState({ password: value })}
                                            returnKeyType='next'
                                            autoCapitalize='none'
                                            secureTextEntry />
                                    </Item>
                                </Form>

                                <View style={{ height: 30 }} />
                                <View style={VIEW_Left}>
                                    <Text style={[TXT_Header, { width: '100%' }]}>Adresse</Text>
                                </View>

                                <Form style={styles.INPUT_Container}>
                                    <Item
                                        floatingLabel
                                        error={this.state.inputPaysError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Pays'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.type._root.focus(); }}
                                            getRef={(input) => { this.pays = input; }}
                                            onChangeText={(value) => this.setState({ pays: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputTypeError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Type'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.number._root.focus(); }}
                                            getRef={(input) => { this.type = input; }}
                                            onChangeText={(value) => this.setState({ type: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputNumberError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='Numéro'
                                            autoCapitalize='none'
                                            keyboardType={'number-pad'}
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.street._root.focus(); }}
                                            getRef={(input) => { this.number = input; }}
                                            onChangeText={(value) => this.setState({ number: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputStreetError}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='rue'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.street1._root.focus(); }}
                                            getRef={(input) => { this.street = input; }}
                                            onChangeText={(value) => this.setState({ street: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputStreet1Error}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='rue1'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            onSubmitEditing={() => { this.street2._root.focus(); }}
                                            getRef={(input) => { this.street1 = input; }}
                                            onChangeText={(value) => this.setState({ street1: value })}
                                            blurOnSubmit={false} />
                                    </Item>
                                    <View style={{ height: 20 }} />
                                    <Item
                                        floatingLabel
                                        error={this.state.inputStreet2Error}
                                        style={{ flexDirection: 'row-reverse' }} >
                                        <Input
                                            placeholder='rue2'
                                            getRef={(input) => { this.street2 = input; }}
                                            onChangeText={(value) => this.setState({ street2: value })}
                                            doneButtonTitle={'OK'}
                                            autoCapitalize='none' />
                                    </Item>

                                </Form>

                                <View style={{ height: 20 }} />
                                <Text style={TXT_Message}>
                                    {this.state.message}
                                </Text>
                                <View style={{ height: 20 }} />
                                {this.spinerRender()}
                            </View>
                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    VIEW_Flex: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR
    },
    VIEW_Center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    VIEW_Left: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        height: 30,
        width: '100%'
    },
    VIEW_Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 40,
        paddingRight: 30,
        paddingBottom: 20,
        paddingTop: 20,
    },
    VIEW_Header: {
        height: 60,
        flexDirection: 'row'
    },
    TXT_Header: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        justifyContent: 'center'
    },
    TXT_Button: {
        alignSelf: 'center',
        color: TEXT_COLOR,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 20,
    },
    TXT_Message: {
        color: ERROR_COLOR,
        fontSize: 16,
        marginTop: 10,
    },
    INPUT_Container: {
        padding: 0,
        alignSelf: 'stretch',
    },
    VIEW_Button: {
        borderRadius: 8,
        height: 40,
        justifyContent: 'center',
        borderColor: BORDER_COLOR,
        borderWidth: 0.5,
    },
    VIEW_ROW: {
        flexDirection: 'row'
    },
    BTN_BACK: {
        margin: 16
    }
});

export default SignupPage;
