import React, { Component } from 'react';
import {
    StyleSheet, View, TouchableOpacity, Platform,
    ScrollView, SafeAreaView, Keyboard, KeyboardAvoidingView, Image, TextInput
} from 'react-native';
import { Form, Input, Item, Text, ActionSheet } from 'native-base';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';

import {
    ALL_FIELDS_REQURED, BACKGROUND_COLOR, TEXT_COLOR, BORDER_COLOR, ERROR_COLOR,
    CameraImage, RecordVideoImage, FileImage
} from '../helper/Constant';

const Options = [
    { text: 'Camera' },
    { text: 'Galary' },
    { text: 'Cancel' }
  ];
class UploadPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comments: '',
            message: '',
            loading: false,
        };
    }

    selectImages() {
        this.openDialog();
    }

    selectVideo() {
        ImagePicker.openPicker({
            mediaType: "video",
          }).then((video) => {
            console.log(video);
          });
    }

    selectDocuments() {

    }

    openDialog() {
        ActionSheet.show(
                  {
                    options: Options,
                    cancelButtonIndex: 2,
                    title: 'Sélectionnez Options'
                  },
                  buttonIndex => {
                    console.log(buttonIndex);
                    if (buttonIndex === 0) {
                      ImagePicker.openCamera({
                        width: 200,
                        height: 200,
                        cropping: true,
                        includeBase64: true,
                        includeExif: true,
                      }).then(image => {

                      }).catch(e => console.log(e));
                    } else if (buttonIndex === 1) {
                      ImagePicker.openPicker({
                        width: 200,
                        height: 200,
                        cropping: true,
                        includeBase64: true,
                        includeExif: true,
                        multiple: true
                      }).then(image => {
                      }).catch(e => console.log(e));
                    }
                  }
                );
      }
      
    spinerRender() {
        if (this.state.loading) {
            return <Spinner size="large" />;
        }
        return (
            <TouchableOpacity onPress={console.log('')}>
                <View style={styles.VIEW_Button}>
                    <Text style={styles.TXT_Button} > Soumettre </Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        const { VIEW_Container, VIEW_Container1, VIEW_Flex, TXT_Message, VIEW_Row, ImageSize, TXT_Header } = styles;
        return (
            <SafeAreaView style={VIEW_Flex}>
                <KeyboardAvoidingView
                    style={VIEW_Flex}
                    enabled
                    behavior={(Platform.OS === 'ios') ? "padding" : null}
                    keyboardVerticalOffset={Platform.select({ ios: 0, android: 500 })}
                >
                    <View style={VIEW_Flex}>
                        <ScrollView>
                            <View style={VIEW_Container}>
                                <View style={{ height: 30 }} />
                                {/* 1 */}
                                <View style={VIEW_Row}>
                                    <View>
                                        <TouchableOpacity onPress={this.selectImages.bind(this)}>
                                            <Image source={CameraImage} style={ImageSize} />
                                        </TouchableOpacity>
                                    </View>
                                    <View><Text style={[TXT_Header, { margin: 10 }]}>Cliquez sur le bouton pour ajouter une photo</Text></View>
                                </View>

                                {/* 2 */}
                                <View style={VIEW_Row}>
                                    <View>
                                        <TouchableOpacity onPress={this.selectVideo.bind(this)}>
                                            <Image source={RecordVideoImage} style={ImageSize} />
                                        </TouchableOpacity>
                                    </View>
                                    <View><Text style={[TXT_Header, { margin: 10 }]}>Cliquez sur le bouton pour ajouter une video</Text></View>
                                </View>

                                {/* 3 */}
                                <View style={VIEW_Row}>
                                    <View>
                                        <TouchableOpacity onPress={this.selectDocuments()}>
                                            <Image source={FileImage} style={ImageSize} />
                                        </TouchableOpacity>
                                    </View>
                                    <View><Text style={[TXT_Header, { margin: 10 }]}>Ajouter un document                          </Text></View>
                                </View>
                            </View>

                            <View style={[VIEW_Container1]}>
                                <View style={[styles.textAreaContainer]} >
                                    <TextInput
                                        style={styles.textArea}
                                        underlineColorAndroid="transparent"
                                        placeholder="Ajoutez vos commentaires"
                                        placeholderTextColor="grey"
                                        numberOfLines={10}
                                        multiline={true}
                                        onChangeText={(value) => this.setState({ comments: value })}
                                        value={this.state.comments}
                                    />
                                </View>
                            </View>

                            <View style={VIEW_Container}>
                                <Text style={TXT_Message}>
                                    {this.state.message}
                                </Text>
                                <View style={{ height: 20 }} />
                                {this.spinerRender()}
                            </View>

                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    VIEW_Flex: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR
    },
    VIEW_Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 40,
        paddingRight: 30,
        paddingBottom: 20,
        paddingTop: 20,
    },
    VIEW_Container1: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    TXT_Button: {
        alignSelf: 'center',
        color: TEXT_COLOR,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 20,
    },
    TXT_Message: {
        color: ERROR_COLOR,
        fontSize: 16,
        marginTop: 10,
    },
    VIEW_Button: {
        borderRadius: 8,
        height: 40,
        justifyContent: 'center',
        borderColor: BORDER_COLOR,
        borderWidth: 0.5,
    },
    VIEW_Row: {
        flexDirection: 'row'
    },
    ImageSize: {
        height: 50,
        width: 50
    },
    TXT_Header: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
    },
    textAreaContainer: {
        borderColor: 'grey',
        borderWidth: 1,
        padding: 5
    },
    textArea: {
        height: 150,
        justifyContent: "flex-start",
        fontSize: 16
    }

});

export default UploadPage;
