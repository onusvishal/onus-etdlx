import React from 'react';
import { Platform } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import LoginForm from '../pages/LoginPage';
import SignupForm from '../pages/SignupPage';
import Dashbord from '../pages/Dashbord';
import UploadPage from '../pages/UploadPage';
// cliquez sur le bouton pour ajouter une photo
// cliquez sur le bouton pour ajouter une vidéo
const Navigator = (props) => {
  return (
    <Router
      barButtonIconStyle={{ tintColor: '#000' }}
      navBarButtonColor='#000'
      navigationBarStyle={{
        ...Platform.select({
          android: {
            //  marginTop: StatusBar.currentHeight
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          }
        })
      }}
    >
      <Scene key="root">
        <Scene
          key="login"
          component={LoginForm} hideNavBar={'true'}
        />
        <Scene
          
          key="SignupPage"
          component={SignupForm} hideNavBar={'true'}
          
        />
        <Scene

          key="Dashbord"
          component={Dashbord} hideNavBar={'true'}

        />
        <Scene
type='reset'
          key="Upload"
          component={UploadPage} hideNavBar={'true'}
          initial={!props.isLogin}
        />
      </Scene>
    </Router>
  );
};

export default Navigator;
